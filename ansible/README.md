# ansible.cfg
* Note the settings here and how this is making your selection of the SSH key and SSH user automatic.
* Note that it is specifying that your inventory file is in the current directory and called inventory

# inventory
* Note that there are three groups here, groups are noted by `[]`.
* The first two groups are hosts designed to be masters and workers.
* For this initial example there will only be master code
* The third group creates an over-arching group just called rke and says the other two are my children.
* Groups bring us a way to tell Ansible what to run on. So if we tell ansible to run on 'rke' it will run on ALL hosts. If we tell Ansible to run on 'rke_masters' then it will run on all hosts listed under 'rke_masters'
* Be sure to replace IPs here as needed

# deploy.yaml
* Notice what this file is doing. It's basically just importing playbooks from another directory. This is a common way to do it so that you're splitting up playbooks by functions and then having a primary playbook that does nothing but import them all

# playbooks/base_config.yaml
* Check ToDos

# playbooks/rke2_master_config.yaml
* Check ToDos