resource "libvirt_volume" "master" {
  count  = var.master_count
  name   = "master-${count.index}-qcow2"
  pool   = libvirt_pool.training.name
  source = "file:///home/colton/images/almalinux8.qcow2"
  format = "qcow2"
}

resource "libvirt_domain" "master" {
  count  = var.master_count
  name   = "master-${count.index+1}"
  memory = "8000"
  vcpu   = 4

  cloudinit = libvirt_cloudinit_disk.commoninit.id

  network_interface {
    network_name = "default"
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = element(libvirt_volume.master.*.id, count.index)
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}
