resource "libvirt_volume" "worker" {
  count  = var.worker_count
  name   = "worker-${count.index}-qcow2"
  pool   = libvirt_pool.training.name
  source = "file:///home/colton/images/almalinux8.qcow2"
  format = "qcow2"
}

resource "libvirt_domain" "worker" {
  count  = var.worker_count
  name   = "worker-${count.index+1}"
  memory = "5000"
  vcpu   = 3

  cloudinit = libvirt_cloudinit_disk.commoninit.id

  network_interface {
    network_name = "default"
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = element(libvirt_volume.worker.*.id, count.index)
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}

